import React, {Component} from 'react'
import "../App.css"

class Timer extends Component{
    constructor(props) {
        super(props)
        this.state = {
            time: 0,
            jam : []
        }
    }
    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({time: this.props.start})
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
        this.jam = setInterval(
            () => this.waktu(),
            1000
        )
    }

    componentDidUpdate() {
        if (this.state.time < 0) {
            clearInterval(this.timerID);
        }
    }

    componentWillMount() {
        clearInterval(this.timerID);
        clearInterval(this.jam);
    }

    tick() {
        this.setState({
            time: this.state.time - 1
        });
    }

    waktu() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = "0" + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = "0" + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = "0" + secs;
        this.setState({jam: [hours,mins,secs]});
    }

    render() {
        return(
            <>
            {this.state.time >= 0 &&
            <div className="widget">
                <h1>
                    Sekarang Jam : {this.state.jam[0]}:{this.state.jam[1]}:{this.state.jam[2]}                        
                </h1> 
                <h1>Hitung Mundur : {this.state.time}</h1>     
            </div>
            }
            </>
        )
    }
}

export default Timer