import React, {useState, useEffect} from "react";
import axios from "axios";
import "../Tugas-14/Hooks.css";

const Hooks = () => {
    const [daftarBuah, setdaftarBuah] = useState(null);
    const [input, setInput] = useState({ nama: "", harga: 0, berat: 0, id:""});
    const [statusForm, setstatusForm] = useState("create");
    

    const handleChange = (event) => {
        setInput({ ...input, [event.target.name]: event.target.value });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (input.nama.replace(/\s/g, "") !== ""){
            if (statusForm === "create"){
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
                    name: input.nama,
                    price: input.harga,
                    weight: input.berat,
                })
                .then((res) => {
                    setdaftarBuah([ ...daftarBuah, {
                            nama: res.data.name,
                            harga: res.data.price,
                            berat: res.data.weight,
                            id: res.data.id,
                        },
                    ]);
                });
            } else if (statusForm === "edit"){
                axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
                    name: input.nama,
                    price: input.harga,
                    weight: input.berat,
                })
                .then((res) => {
                    const buah = daftarBuah.filter((element) => element.id === res.data.id);
                    setdaftarBuah([ ...buah, {
                            nama: res.data.name,
                            harga: res.data.price,
                            berat: res.data.weight,
                            id: res.data.id,
                        },
                    ]);
                    console.log(res.data);
                });
            }
            setstatusForm("create");
        }

        setInput({ nama: "", harga: 0, berat:0, id: "" });
    };

    const deleteBuah = (event) => {
        let id = parseInt(event.target.value);
        setdaftarBuah(daftarBuah.filter((element) => element.id !== id));
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then((res) => {
            console.log(res);
        });
    };

    const editBuah = (event) => {
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${event.target.value}`)
        .then((res) => {
            const buah = {
                nama: res.data.name,
                harga: res.data.price,
                berat: res.data.weight,
                id: res.data.id,
            };
            setInput({ ...buah });
        });
        setstatusForm("edit");
    };

    useEffect(() => {
        if (daftarBuah === null){
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then((res) => {
                const buah = res.data.map((element) => {
                    return {
                        nama: element.name,
                        harga: element.price,
                        berat: element.weight,
                        id: element.id,
                    };
                })
                .filter((element) => element.nama !== null);
                setdaftarBuah(buah);
            });
        }
    }, [daftarBuah]);

    return (
        <div className="list">
            {/*Tabel Harga Buah*/}
            <h1 align="center">Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarBuah !== null && daftarBuah.map((element, i) => {
                        return (
                            <tr key={element.id}>
                                <td>{++i}</td>
                                <td>{element.nama}</td>
                                <td>{element.harga}</td>
                                <td>{element.berat / 1000} Kg</td>
                                <td>
                                    <button value={element.id} onClick={editBuah}>Edit</button>&nbsp;
                                    <button value={element.id} onClick={deleteBuah}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            
            {/*Form Edit/Tambah */}
            <h1>Form Harga Buah</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Masukkan Nama Buah</label>
                    <input
                        type="text"
                        value={input.nama}
                        onChange={handleChange}
                        name="nama"
                    />
                </div>
                <div>
                    <label>Masukkan Harga Buah</label>
                    <input
                        type="number"
                        value={input.harga}
                        onChange={handleChange}
                        name="harga"
                    />
                </div>
                <div>
                    <label>Masukkan Berat Buah</label>
                    <input
                        type="number"
                        value={input.berat}
                        onChange={handleChange}
                        name="berat"
                    />
                </div>
                <button>Apply</button>
            </form>
        </div>
    );
};

export default Hooks;