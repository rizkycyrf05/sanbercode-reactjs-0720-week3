import React from "react";
import Tugas11 from "../Tugas-11/Tugas11";
import Timer from "../Tugas-12/Timer";
import Lists from "../Tugas-13/Lists";
import Hooks from "../Tugas-14/Hooks";
import Fruit from "../Tugas-15/Fruit";
import Home from "../Tugas-15/Home";
import { Switch, Route } from "react-router";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/Tugas11">
        <Tugas11 />
      </Route>
      <Route exact path="/Timer">
        <Timer start="100" />
      </Route>
      <Route exact path="/Lists">
        <Lists />
      </Route>
      <Route exact path="/Hooks">
        <Hooks />
      </Route>
      <Route exact path="/Fruit">
        <Fruit />
      </Route>
    </Switch>
  );
};

export default Routes;
