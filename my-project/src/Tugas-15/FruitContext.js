import React, { useState, createContext } from "react";

export const FruitContext = createContext();

export const FruitProvider = (props) => {
  const [daftarBuah, setDaftarBuah] = useState(null);
  const [idBuah, setIdBuah] = useState("");

  return (
    <FruitContext.Provider
      value={[daftarBuah, setDaftarBuah, idBuah, setIdBuah]}
    >
      {props.children}
    </FruitContext.Provider>
  );
};
