import React, { useContext, useEffect } from "react";
import { FruitContext } from "./FruitContext";
import axios from "axios";
import "./Fruit.css";

const FruitList = () => {
    const [daftarBuah, setdaftarBuah, idBuah, setIdBuah] = useContext(
        FruitContext
    );

  const deleteBuah = (e) => {
        console.log(e);
        let id = parseInt(e.target.value);
        setdaftarBuah(daftarBuah.filter((el) => el.id !== id));
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then((res) => {
        console.log(res);
      });
    };

    const editBuah = (e) => {
        setIdBuah(e.target.value);
        console.log(idBuah);
    };

    useEffect(() => {
        if (daftarBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then((res) => {
                const buah = res.data
                .map((el) => {
                    return {
                        nama: el.name,
                        harga: el.price,
                        berat: el.weight,
                        id: el.id,
                    };
                })
                .filter((el) => el.nama !== null);
                setdaftarBuah(buah);
            });
        }
    });

    return (
        <div className="list">
        <h1 align="center">Tabel Harga Buah</h1>
        <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {daftarBuah !== null &&
                daftarBuah.map((el, i) => {
                    return (
                        <tr key={el.id}>
                            <td>{++i}</td>
                            <td>{el.nama}</td>
                            <td>{el.harga}</td>
                            <td>{el.berat / 1000} Kg</td>
                            <td>
                                <button value={el.id} onClick={editBuah}>
                                    Edit
                                </button>{" "}
                                <button value={el.id} onClick={deleteBuah}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
};

export default FruitList;
