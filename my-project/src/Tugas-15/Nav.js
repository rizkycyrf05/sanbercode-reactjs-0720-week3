import React, { useState } from "react";
import "../Tugas-15/Fruit.css";
import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <>
      <nav style={{ backgroundColor: "#ffaa2b" }}>
        <ul>
          <li>
            <Link to="/" style={{ color: "#fff" }}>
              Home
            </Link>
          </li>
          <li>
            <Link to="/Tugas11" style={{ color: "#fff" }}>
              Tugas 11
            </Link>
          </li>
          <li>
            <Link to="/Timer" style={{ color: "#fff" }}>
              Tugas 12
            </Link>
          </li>
          <li>
            <Link to="/Lists" style={{ color: "#fff" }}>
              Tugas 13
            </Link>
          </li>
          <li>
            <Link to="/Hooks" style={{ color: "#fff" }}>
              Tugas 14
            </Link>
          </li>
          <li>
            <Link to="/Fruit" style={{ color: "#fff" }}>
              Tugas 15
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Nav;
