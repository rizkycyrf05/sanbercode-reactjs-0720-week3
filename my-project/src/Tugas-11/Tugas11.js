import React from "react";

class Header extends React.Component {
  render() {
    return <h1 style={{textAlign: "center"}}>{this.props.title}</h1>;
  }
}
class Table extends React.Component {
  render() {
    return <table style={{border: "1px solid #000",margin: "0 auto", width: "70%"}}>
      <tr>
        <th style={{backgroundColor: "#b3aeae"}}>Nama</th>
        <th style={{backgroundColor: "#b3aeae"}}>Harga</th>
        <th style={{backgroundColor: "#b3aeae"}}>Berat</th>
      </tr>
      {this.props.data.map(column => {
        return (
          <tr>
            <td style={{background: "#f08054"}}>{column.nama}</td>
            <td style={{background: "#f08054"}}>{column.harga}</td>
            <td style={{background: "#f08054"}}>{column.berat/1000} kg</td>
          </tr>
        )
      })}
    </table>
  }
}
let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]
class Tugas11 extends React.Component {
  render() {
    return (
      <>
      <Header title="Tabel Harga Buah" />
      <Table data={dataHargaBuah}/>
      </>
    )
  }
}

export default Tugas11;
