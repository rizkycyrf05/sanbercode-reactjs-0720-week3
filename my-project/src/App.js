import React from "react";
//import Tugas11 from "./Tugas-11/Tugas11";
//import Timer from "./Tugas-12/Timer";
//import Lists from "./Tugas-13/Lists";
//import Hooks from "./Tugas-14/Hooks";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Tugas-15/Routes";
import Nav from "./Tugas-15/Nav";
import "./App.css";


function App() {
  return (
    <>
      {/*Tugas-11*/}
      {/*<Tugas11 />*/}

      {/*Tugas-12*/}
      {/*<Timer start ="100"/>*/}

      {/*Tugas-13*/}
      {/*<Lists />*/}

      {/*Tugas-14*/}
      {/*<Hooks/>*/}

      {/*Tugas-15*/}
      <Router>
        <Nav/>
        <Routes/>
      </Router>
    </>
  );
}

export default App;
