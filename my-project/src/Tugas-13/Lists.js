import React, {Component} from "react"

class Lists extends Component{

  constructor(props){
    super(props)
    this.state ={
      daftarBuah : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      nama: "",
      harga: 0,
      berat: 0,
      index: -1
    }
    this.deleteBuah = this.deleteBuah.bind(this);
    this.editBuah = this.editBuah.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    if(event.target.name === "nama") this.setState({nama: event.target.value });
    else if(event.target.name === "harga") this.setState({harga: event.target.value});
    else if(event.target.name === "berat") this.setState({berat: event.target.value});
  }

  handleSubmit(event){
    event.preventDefault()
    let newBuah = [];
    const addBuah = {
      nama: this.state.nama,
      harga: this.state.harga,
      berat: this.state.berat
    }
    if (this.state.index === -1) {
      newBuah = [...this.state.daftarBuah, addBuah]
    }else{
      this.state.daftarBuah[this.state.index] = addBuah;
      newBuah = this.state.daftarBuah;
    }
    this.setState({
      daftarBuah: newBuah,
      nama: "",
      harga: 0,
      berat: 0,
      index: -1
    });
  }

  deleteBuah(e){
    const newBuah = this.state.daftarBuah;
    newBuah.splice(e.target.value, 1);
    this.setState({
      daftarBuah: newBuah
    });
  }

  editBuah(e){
    const newBuah = this.state.daftarBuah[e.target.value];
    this.setState({
      nama: newBuah.nama,
      harga: newBuah.harga,
      berat: newBuah.berat,
      index: e.target.value
    })
  }
  render(){
    return(
      <>
        <h1 align="center">Tabel Harga Buah</h1>
        <table style={{ border: "1px solid #000", margin: "0 auto", width: "80%"}}>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Opsi</th>
            </tr>
          </thead>
          <tbody>
            {
            this.state.daftarBuah.map((el,i) => {
              return (
                <tr>
                  <td>{el.nama}</td>
                  <td>{el.harga}</td>
                  <td>{el.berat / 1000} Kg</td>
                  <td align = "center"><button onClick={this.editBuah} value={i}>Edit</button> Atau <button value={i} onClick={this.deleteBuah}>Delete</button></td>
                </tr>
              );
            })
            }
          </tbody>
        </table>
        
        <form onSubmit={this.handleSubmit}>
            <table style = {{ border: "1px solid #000", margin: "0 auto", width: "80%"}}>
            <h1>Edit/Tambah</h1>
            <tr>
                <td><label>Masukkan Nama Buah</label></td>
                <td><label>Masukkan Harga Buah</label></td>
                <td><label>Masukkan Berat Buah</label></td>
            </tr>
            <tr>
                <td class = "Box"><input type="text" value={this.state.nama} onChange={this.handleChange} name="nama"/></td>
                <td class = "Box"><input type="number" value={this.state.harga} onChange={this.handleChange} name="harga"/></td>
                <td class = "Box"><input type="number" value={this.state.berat} onChange={this.handleChange} name="berat"/></td>
            </tr>  
            <button>Apply</button>    
            </table>         
        </form>
      </>
    )
  }
}

export default Lists
